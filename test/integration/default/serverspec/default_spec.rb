require 'spec_helper'

describe 'chef-tutorial::default' do
  # Serverspec examples can be found at
  # http://serverspec.org/resource_types.html

  describe file('/var/www/online-app') do
    it { should be_directory }
  end
  describe file('/var/www/online-app/index.html') do
    it { should be_file }
  end
end
