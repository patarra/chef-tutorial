#
# Cookbook Name:: chef-tutorial
# Recipe:: default
#

include_recipe 'chef-tutorial::install_apache'
package 'unzip' do
  action :install
end
# A directory is required to install the app files
directory '/var/www/online-app' do
  owner 'root'
  group 'root'
  mode 00755
  recursive true
  action :create
end

# download the app
remote_file "#{Chef::Config[:file_cache_path]}/pm-online.zip" do
  source 'http://10.0.0.119:9090/pm-online.zip'
  notifies :run, 'script[install-unzip-app-zipped]', :immediately
end

# unzip de remote file an update contents
script 'install-unzip-app-zipped' do
  interpreter 'bash'
  user 'root'
  cwd '/tmp'
  code <<-EOF
    rm -rf /var/www/online-app/*
    unzip -d /var/www/online-app #{Chef::Config[:file_cache_path]}/pm-online.zip
  EOF
  action :nothing
end

# Configuration of the vhost
template '/etc/httpd/conf.d/ports.conf' do
  source 'apache-ports.erb'
  mode 00644
  notifies :reload, 'service[httpd]', :delayed
end

template '/etc/httpd/conf.d/pm-online.conf' do
  source 'vhost.erb'
  mode 00644
  notifies :reload, 'service[httpd]', :delayed
end

service 'httpd' do
  action :nothing
end
